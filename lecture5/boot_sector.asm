; OS Dev Lecture 5
[org 0x7c00]

; Set the stack away from our code
mov bp, 0x8000 ; Set the stack base (bottom)
mov sp, bp ; Set the stack top to the base (an empty stack)


mov si, HELLO_MSG ; Move HELLO_MESSAGE address into si register
call print_string

mov si, GOODBYE_MSG
call print_string

jmp $


%include "my_functions.asm"
;
; Functions
;

;print_string:
  
;  _loop:
;    ;mov al, [si]
;    lodsb ; String optimized instruction
;    cmp al, 0
;    je _end

;    mov ah, 0x0e ; BIOS teletype function
;    int 0x10
;    ;inc si
;  jmp _loop
  
;  _end:
;    ret


;print_char:
;  mov ah, 0x0e ; BIOS teletype function
;  int 0x10
;  ;pusha
;  ;popa
;  ret

;
; Data
;

HELLO_MSG:
  db 'Hello',0

GOODBYE_MSG:
  db 'Goodbye',0

times 510-($-$$) db 0
dw 0xaa55
