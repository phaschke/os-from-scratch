; OS Dev Lecture 5
[org 0x7c00]

; Set the stack away from our code
mov bp, 0x8000 ; Set the stack base (bottom)
mov sp, bp ; Set the stack top to the base (an empty stack)

;mov dx, 0x1234
;mov dx, [0x7c00+510]
;call print_hex

mov ax, 0xc000
mov ds, ax
mov si, [0x0002]
call print_string

jmp $

call find_bios_string

;find_me:
;  db 'BIOS',0

jmp $ ; hang CPU here


find_bios_string:
  
  mov bx, 0  ; bx -> 0
  mov es, bx ; es -> 0

  _search_loop:
    
    mov al, [es:bx]
    cmp al, 'B'
    jne _continue_search
    
    mov al, [es:bx+1]
    cmp al, 'I'
    jne _continue_search
    
    mov al, [es:bx+2]
    cmp al, 'O'
    jne _continue_search
    
    mov al, [es:bx+3]
    cmp al, 'S'
    jne _continue_search

    mov dx, es
    call print_hex

    mov dx, bx
    call print_hex
    jmp $

    _continue_search:

    inc bx ; 0xffff -> 0x0000 rollover
    cmp bx, 0
    je _increment_segment


    jmp _search_loop

    _increment_segment:
      mov cx, es
      add cx, 0x1000
      mov es, cx
      jmp _search_loop
  
  ret




;
; Functions
;
;print_hex_new:
;  mov si, HEX_TEMPLATE
;  push cx
;  mov cx, 12
;  cmp cx, 0
;  jl _end
;  mov bx, dx
;  shr bx, [cx]
;  cmp cx, 12
;  jl _mask
;  _continue:
;    mov bx, [bx+HEXABET]
;    mov [HEX_TEMPLATE+]
;
;  _mask:
;    and bx, 0x000f
;    jmp _continue
;
;  _end:
;
;  ret


print_hex:
  push bx
  push si
  mov si, HEX_TEMPLATE
  
  mov bx, dx     ; bx -> 0x1234
  shr bx, 12     ; bx -> 0x0001
  mov bx, [bx+HEXABET]
  mov [HEX_TEMPLATE+2], bl
  
  mov bx, dx     ; bx -> 0x1234
  shr bx, 8      ; bx -> 0x0012
  and bx, 0x000f ; bx -> 0x0002 mask last char
  mov bx, [bx+HEXABET]
  mov [HEX_TEMPLATE+3], bl
  
  mov bx, dx     ; bx -> 0x1234
  shr bx, 4      ; bx -> 0x0123
  and bx, 0x000f ; bx -> 0x0003 
  mov bx, [bx+HEXABET]
  mov [HEX_TEMPLATE+4], bl
  
  mov bx, dx     ; bx -> 0x1234
  and bx, 0x000f ; bx -> 0x0004 
  mov bx, [bx+HEXABET]
  mov [HEX_TEMPLATE+5], bl
  
  call print_string

  pop si
  pop bx
  ret

; include print_string
%include "my_functions.asm"


;
; Data
;

HEX_TEMPLATE:
 db '0x???? ',0

HEXABET:
  db '0123456789abcdef'

times 510-($-$$) db 0
dw 0xaa55
