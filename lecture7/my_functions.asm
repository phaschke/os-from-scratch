;
; Functions
;

print_string:
  
  _loop:
    ;mov al, [si]
    lodsb ; String optimized instruction
    cmp al, 0
    je _end

    mov ah, 0x0e ; BIOS teletype function
    int 0x10
    ;inc si
  jmp _loop
  
  _end:
    ret

; Print value in dx as hex
print_hex:
  push bx
  push si
  mov si, HEX_TEMPLATE
  
  mov bx, dx     ; bx -> 0x1234
  shr bx, 12     ; bx -> 0x0001
  mov bx, [bx+HEXABET]
  mov [HEX_TEMPLATE+2], bl
  
  mov bx, dx     ; bx -> 0x1234
  shr bx, 8      ; bx -> 0x0012
  and bx, 0x000f ; bx -> 0x0002 mask last char
  mov bx, [bx+HEXABET]
  mov [HEX_TEMPLATE+3], bl
  
  mov bx, dx     ; bx -> 0x1234
  shr bx, 4      ; bx -> 0x0123
  and bx, 0x000f ; bx -> 0x0003 
  mov bx, [bx+HEXABET]
  mov [HEX_TEMPLATE+4], bl
  
  mov bx, dx     ; bx -> 0x1234
  and bx, 0x000f ; bx -> 0x0004 
  mov bx, [bx+HEXABET]
  mov [HEX_TEMPLATE+5], bl
  
  call print_string

  pop si
  pop bx
  ret

print_char:
  mov ah, 0x0e ; BIOS teletype function
  int 0x10
  ;pusha
  ;popa
  ret

HEX_TEMPLATE:
 db '0x???? ',0

HEXABET:
  db '0123456789abcdef'

