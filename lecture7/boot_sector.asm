; OS Dev Lecture 7
[org 0x7c00]

; Set the stack away from our code
mov bp, 0xffff ; Set the stack base (bottom)
mov sp, bp ; Set the stack top to the base (an empty stack)

call print_hex

call read_from_disk

mov si, MY_MESSAGE
call print_string


jmp $ ; Hang

; Include util functions print_string, print_hex
%include "my_functions.asm"

read_from_disk:

  mov ah, 0x02 ; Read sectors from disk
  
  mov al, 1 ; Numbers of sectors to read
  mov ch, 0 ; Select first cylinder/track
  mov dh, 0 ; Select first read/write head
  mov cl, 2 ; Select second sector

  mov bx, 0
  mov es, bx ; Not applying segment offset, es -> 0
  mov bx, 0x7c00 + 512

  jc read_error ; Jump to error message if carry flag set by BIOS

  int 0x13

  ret

read_error:
  mov si, DISK_ERROR
  call print_string
  jmp $

DISK_ERROR:
  db 'Error reading Disk!',0

; Pad out the bootsector to 512 bytes with last 2 bytes as magic 'bootable' number.
times 510-($-$$) db 0
dw 0xaa55

MY_MESSAGE:
  db 'Am I loaded in memory yet?',0

