; OS Dev Lecture 9
[bits 16]
[org 0x7c00]

; Set the stack away from our code
mov bp, 0x8000 ; Set the stack base (bottom)
mov sp, bp ; Set the stack top to the base (an empty stack)

mov si, MSG_REAL_MODE
call print_string

mov si, MSG_LOADING_KERNEL
call print_string

;mov al, 9 ; Number of sectors to load - 9 is max we can move before we have to load another cylinder
;mov bx, 0x7e00 ; (aka 0x7e00) Address to load kernel entry
;call read_from_disk
  pusha

  mov ah, 0x02 ; Read sectors from disk
  
  mov al, 9
  mov ch, 0 ; Select first cylinder/track
  mov dh, 0 ; Select first read/write head
  mov cl, 2 ; Select second sector

  push bx
  mov bx, 0
  mov es, bx ; Not applying segment offset, es -> 0
  pop bx
  mov bx, kernel_entry

  int 0x13

  jc disk_read_error ; Jump to error message if carry flag set by BIOS

  popa

mov si, MSG_LOADED_KERNEL
call print_string

; Switch into protected mode
call switch_to_pm

jmp $ ; Hang - if switch fails.


%include "includes/my_functions.asm"
%include "includes/switch_to_pm.asm"
%include "includes/my_pm_functions.asm"

[bits 32]

BEGIN_PM:

  mov esi, MSG_PROT_MODE
  call print_string_pm

  ;%include "includes/kernel.asm"
  
  ; jmp to loaded kernel code
  jmp kernel_entry

  ;jmp $

[bits 16]

; Messages to display
MSG_REAL_MODE db "Started in 16-bit Real Mode",0
MSG_LOADING_KERNEL db "Loading Kernel from disk to address 0x7e00",0
MSG_LOADED_KERNEL db "Loaded Kernel from disk to address 0x7e00",0
MSG_PROT_MODE db "Successfully Switched into 32-bit Protected Mode",0

; Pad out the bootsector to 512 bytes with last bytes as magic 'bootable' number.
times 510-($-$$) db 0
dw 0xaa55

kernel_entry:
  ; Kernel code will be loaded here - after magic number.
