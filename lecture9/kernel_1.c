
#define VIDEO_MEM_ADDR 0xb8000
#define LINES 25
#define COLUMNS_IN_LINE 80

#define IDT_SIZE 256


//extern void keyboard_handler(void);

void kmain();


void entry() {
        kmain();
}

struct IDT_entry {
        unsigned short int offset_lowerbits;
        unsigned short int selector;
        unsigned char zero;
        unsigned char type_attr;
        unsigned short int offset_higherbits;
};

struct IDT_entry IDT[IDT_SIZE];

void idt_init(void) {
        unsigned long keyboard_address;
        unsigned long idt_address;
        unsigned long idt_ptr[2];

        //Populate IDT entry of keyboard's interrupt
        //keyboard_address = (unsigned long)keyboard_handler;
        //IDT[0x21].offset_lowerbits = keyboard_address & 0xffff;

}

void print(char* message) {
        char *p_video_buffer = (char*) VIDEO_MEM_ADDR;
        char *p_next_char = message;
        while (*p_next_char) {
                *p_video_buffer = *p_next_char;
                p_next_char++;
                p_video_buffer += 2;
        }

}

void kmain() {
        print("The Kernel is now executing!");
        while (1) {
        }

}

// Set up 32 bit interupt table
// Display CPU time
// Device drivers -> keyboard
// Get user input
