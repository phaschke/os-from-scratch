[bits 16] ; Instruct nasm to output 16-bit optcodes

; Include out prepared global discriptor table structre
%include "includes/gdt.asm"

; Switch to protected mode
switch_to_pm:

  cli                   ; We must switch off interrupts until we have
                        ; set up the protect mode interrupt vector
                        ; otherwise interrupts will run amuck.

  lgdt [gdt_descriptor] ; Load our global discriptor table, which defines
                        ; the protected mode segments (e.g. for code and data)

  mov eax, cr0         ; To make the switch to protected mode, we set
  or eax, 0x1           ; the first bit of CR0, a special CPU control register
  mov cr0, eax

  jmp CODE_SEG:init_pm  ; Make a far jump (i.e. to a new segment) to out 32-bit
                        ; code. This also forces the CPU to flush its cache of
                        ; pre-fetched and real-mode decoded instructions, which can
                        ; cause problems for certain processors.

[bits 32] ; Instruct nasm to output 32-bit opcodes

; Initalise registers and the stack once in PM.
init_pm:

  mov ax, DATA_SEG      ; Now in PM, our old segments are meaningless,
  mov ds, ax            ; so we point our segment registers to the
  mov ss, ax            ; data selector we defined in out GDT
  mov es, ax
  mov fs, ax
  mov gs, ax

  mov ebp, 0x90000      ; Update out stack potisition so it is right
  mov esp, ebp          ; at the top of free space

  call BEGIN_PM         ; Finally, call some well-known label, where
                        ; general 32-bit code may take over to boot the 32bit OS.

[bits 16] ; Instruct nasm to output 16-bit opcodes
