[bits 32]

global keyboard_handler

extern keyboard_handler_main


; Define keyboard handler
keyboard_handler:
        call keyboard_handler_main
        iretd
