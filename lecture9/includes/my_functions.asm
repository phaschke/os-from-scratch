;
; Functions
;

print_string:
  
  _loop:
    ;mov al, [si]
    lodsb ; String optimized instruction
    cmp al, 0
    je _end

    mov ah, 0x0e ; BIOS teletype function
    int 0x10
    ;inc si
  jmp _loop
  
  _end:
    ret


print_char:
  mov ah, 0x0e ; BIOS teletype function
  int 0x10
  ;pusha
  ;popa
  ret

;
; Disk Reading
; Params: 
;  al - Numbers of sectors to read
;  bx - Address to load it in
;

read_from_disk:
  pusha

  mov ah, 0x02 ; Read sectors from disk
  
  ;mov al, 1 ; Numbers of sectors to read (Commented here so it can be set as an argument)
  mov ch, 0 ; Select first cylinder/track
  mov dh, 0 ; Select first read/write head
  mov cl, 2 ; Select second sector

  push bx
  mov bx, 0
  mov es, bx ; Not applying segment offset, es -> 0
  pop bx
  ;mov bx, 0x7c00 + 512 (comment so caller can set as an arg)

  int 0x13

  jc disk_read_error ; Jump to error message if carry flag set by BIOS

  popa
  ret


disk_read_error:
  mov si, DISK_ERROR
  call print_string
  jmp $

DISK_ERROR: db 'Error reading Disk!',0

