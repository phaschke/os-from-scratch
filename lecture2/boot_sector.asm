[org 0x7c00] ; Treat the start of out boot sector as the start address

; mov [0x40], word 0x0

mov al, [my_character]
mov ah, 0x0e
int 0x10

my_character:
db 'X'

jmp $ ; endless loop. keep random data form being read

; Another comment
times 510-($-$$) db 0 ; Take 512 - the offest and write zeros
dw 0xaa55 ; Declare a word (2 bytes) magic number







; ax, bx, cx, dx General purpose registers
; mov dl, 0x12 move to low bytes
; mov dh, 0x34 move high

; Absolute addressing example 
;mov ax, 0x7c0
;mov ds, ax


;mov al, [0xc] ; 0x7c0 * 0x10 -> 0x7c00 + 0xc -> 0x7c0c
;mov ah, 0x0e ; BIOS scrolling teletype
;int 0x10 ; interupt code

