[org 0x7c00]


mov ah, 0x0e
mov al, 'A'

loop:
  int 0x10
  add al, 1 
  cmp al, 'Z'
  jle loop



; -------------------------------------------
; If statements

;mov ah, 0x0e ; BIOS scrolling teletype function

;mov dx, 9
; if dx < 10 -> print 't' 
; else -> print 'T'

;cmp dx, 10
;jl less_than_10
;mov al, 'T'
;jmp endif ; without this labels will execute still

;less_than_10:
;  mov al, 't'

;endif:

;int 0x10


;------------------------------------------
; Stack

;mov bp, 0x8000
;mov sp, bp

; Push characters
;push 'A'
;push 'B'
;push 'C'

;mov al, [0x7ffe] ; each item on the stack is 2 bytes, growing downwards
;int 0x10 ; print al


;jmp $

;  Pop and print
;pop bx
;mov al, bl
;int 0x10 ; print al

;pop bx
;mov al, bl
;int 0x10 ; print al

;pop bx
;mov al, bl
;int 0x10 ; print al

;-----------------------------------

jmp $ ; Endless jump to stop the CPU from executing arbitrary bytes in memory as code

times 510-($-$$) db 0
dw 0xaa55
