; OS Dev Lecture 8
[org 0x7c00]

; Set the stack away from our code
mov bp, 0x8000 ; Set the stack base (bottom)
mov sp, bp ; Set the stack top to the base (an empty stack)

mov si, MSG_REAL_MODE
call print_string

; Switch into protected mode
call switch_to_pm

jmp $ ; Hang


%include "includes/my_functions.asm"
%include "includes/switch_to_pm.asm"
%include "includes/my_pm_functions.asm"

[bits 32]

BEGIN_PM:

; 0xb8000 Video memory location, write directly to video memory
; mov edx, 0xb8000
; mov [edx], byte 'A'

mov esi, MSG_PROT_MODE
call print_string_pm
  
jmp $

[bits 16]

; Messages to display
MSG_REAL_MODE db "Started in 16-bit Real Mode",0
MSG_PROT_MODE db "Successfully Switched into 32-bit Protected Mode",0

; Pad out the bootsector to 512 bytes with last bytes as magic 'bootable' number.
times 510-($-$$) db 0
dw 0xaa55
