; OS Dev Book Example
[org 0x7c00]
KERNEL_OFFSET equ 0x1000 ; This is the memory offset to which we will load our kernel

mov [BOOT_DRIVE], dl    ; BIOS stores out bootdrive in DL, so it's best to remember
                        ; this for later.

; Set the stack away from our code
mov bp, 0x9000 ; Set the stack base (bottom)
mov sp, bp ; Set the stack top to the base (an empty stack)

mov si, MSG_REAL_MODE
call print_string

call load_kernel

;mov al, 9 ; Number of sectors to load - 9 is max we can move before we have to load another cylinder
;mov bx, 0x7e00 ; (aka 0x7e00) Address to load kernel entry
;call read_from_disk
  ;pusha

  ;mov ah, 0x02 ; Read sectors from disk
  
  ;mov al, 9
  ;mov ch, 0 ; Select first cylinder/track
  ;mov dh, 0 ; Select first read/write head
  ;mov cl, 2 ; Select second sector

  ;push bx
  ;mov bx, 0
  ;mov es, bx ; Not applying segment offset, es -> 0
  ;pop bx
  ;mov bx, kernel_entry

  ;int 0x13

  ;jc disk_read_error ; Jump to error message if carry flag set by BIOS

  ;popa

; Switch into protected mode
call switch_to_pm

jmp $ ; Hang - if switch fails.

; Include useful routines
%include "boot/includes/my_functions.asm"
%include "boot/disk/disk_load.asm"
%include "boot/includes/switch_to_pm.asm"
%include "boot/includes/my_pm_functions.asm"

[bits 16]

;load_kernel
load_kernel:
  mov si, MSG_LOADING_KERNEL ; Print a message to say we are loading the kernel
  call print_string

  mov bx, KERNEL_OFFSET ; Set up parameters for our disk_load routine, so 
  mov dh, 15            ; that we load the first 15 sectors ( excluding
  mov dl, [BOOT_DRIVE]  ; the boot sector) from the boot disk ( i.e. your
  call disk_load        ; kernel code) to address KERNEL_OFFSET

  mov si, MSG_LOADED_KERNEL
  call print_string

  ret

[bits 32]

BEGIN_PM:

  mov esi, MSG_PROT_MODE
  call print_string_pm
  
  ; jmp to loaded kernel code
  call KERNEL_OFFSET

  ;jmp $

; Global Variables
BOOT_DRIVE         db 0
MSG_REAL_MODE      db "Started in 16-bit Real Mode",0
MSG_LOADING_KERNEL db "Loading Kernel from disk to address 0x7e00",0
MSG_LOADED_KERNEL  db "Loaded Kernel from disk to address 0x7e00",0
MSG_PROT_MODE      db "Successfully Switched into 32-bit Protected Mode",0

; Pad out the bootsector to 512 bytes with last bytes as magic 'bootable' number.
times 510-($-$$) db 0
dw 0xaa55

;kernel_entry:
  ; Kernel code will be loaded here - after magic number.
