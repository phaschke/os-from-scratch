#include "../cpu/isr.h"
#include "../cpu/idt.h"
#include "../drivers/ports.h"
#include <stdint.h>

#define IDT_SIZE 256
#define KEYBOARD_DATA_PORT 0x60
#define KEYBOARD_STATUS_PORT 0x64
#define INTERRUPT_GATE 0x8e

#define ENTER_KEY_CODE 0x1C

//extern void keyboard_handler(void);
//extern char read_port(unsigned short port);
//extern void write_port(unsigned short port, unsigned char data);
//extern void load_idt(unsigned long *idt_ptr);

void keyboard_handler_main(registers_t *regs);
void init_keyboard();


//void idt_init();
//void kb_init();

