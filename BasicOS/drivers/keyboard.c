#include "keyboard.h"
#include "screen.h"
#include "includes/keyboard_map.h"

//struct IDT_entry {
//        unsigned short int offset_lowerbits;
//        unsigned short int selector;
//        unsigned char zero;
//        unsigned char type_attr;
//        unsigned short int offset_higherbits;
//};

//struct IDT_entry IDT[IDT_SIZE];

/* Key status byte
 * bit 0, LShift
 * bit 1, RShift
 * bit 2, alt
 * bit 3, control
 * bit 4, capslock
 * bit 5, numlock
 * bit 6, scrolllock
 */
uint8_t key_status = 0x0;


void keyboard_handler_main(registers_t *regs) { 
        //unsigned char status;
        uint8_t keycode;

        // Write EOI
        //write_port(0x20, 0x20);

        // status port is 0x64
        //status = read_port(KEYBOARD_STATUS_PORT);
        uint8_t status = port_byte_in(KEYBOARD_STATUS_PORT);

        if(status & 0x01) {
                // Data port is 0x60
                // Highest bit set on release
                //uint8_t keycode = read_port(KEYBOARD_DATA_PORT);
                keycode = port_byte_in(KEYBOARD_DATA_PORT);
                if(keycode < 0) return;
                
                // Key was released
                if(keycode & 0x80) {
                        // Flip last byte for char reading
                        keycode ^= 0x80;


                        switch(keycode) {
                                // LShift clear status bit
                                case 0x2A:
                                        key_status &= ~0x01;
                                        //print_char('-', -1, -1, 0x0f);
                                        //print_char('L', -1, -1, 0x0f);
                                break;
                                // RShift clear status bit
                                case 0x36:
                                        key_status &= ~0x02;
                                        //print_char('-', -1, -1, 0x0f);
                                        //print_char('R', -1, -1, 0x0f);
                                break;

                                default:
                                        return;
                        }
                        return;
                
                } else {
                // Key was pressed
                        print_char(keycode, -1, -1, WHITE_ON_BLACK);
                        
                        switch(keycode) {
                                // LShift set status bit
                                case 0x2A:
                                        key_status |= 0x01;
                                        //print_char('+', -1, -1, 0x0f);
                                        //print_char('L', -1, -1, 0x0f);
                                        return;
                                break;
                                // RShift set status bit
                                case 0x36:
                                        key_status |= 0x02;
                                        //print_char('R', -1, -1, 0x0f);
                                        return;
                                break;
                                // Backspace
                                //if(keycode == 0x0e) {
                                //        print_char('b', -1, -1, 0x0f);
                                //}
                        }

                        if(key_status & 0x01) {
                                //print_char('=', -1, -1, 0x0f);
                                //print_char('L', -1, -1, 0x0f);
                                print_char(u_keyboard_map[(unsigned char) keycode], -1, -1, WHITE_ON_BLACK);
                        }
                        else if(key_status & 0x02) {
                                //print_char('=', -1, -1, 0x0f);
                                //print_char('R', -1, -1, 0x0f);
                                print_char(u_keyboard_map[(unsigned char) keycode], -1, -1, WHITE_ON_BLACK);
                        } else {
                                // Print plain character
                                print_char(l_keyboard_map[(unsigned char) keycode], -1, -1, WHITE_ON_BLACK);

                        }

                        return;
                        

                }
        // Lowest bit of status will be set if the buffer is not empty
        }
}

/*void idt_init(void) {
        unsigned long keyboard_address;
        unsigned long idt_address;
        unsigned long idt_ptr[2];

        //Populate IDT entry of keyboard's interrupt
        keyboard_address = (unsigned long)keyboard_handler;
        IDT[0x21].offset_lowerbits = keyboard_address & 0xffff;
        IDT[0x21].selector = 0x08; // KERNEL_CODE_SEGMENT_OFFSET
        IDT[0x21].zero = 0;
        IDT[0x21].type_attr = 0x8e; // INTERRUPT_GATE
        IDT[0x21].offset_higherbits = (keyboard_address & 0xffff0000) >> 16;
        
        
        //     Ports
        //         PIC1 PCI2
        // Command 0x20 0xA0
        // Data    0x21 0xA1
         
        
        // ICW1 - begin initialization
        write_port(0x20, 0x11);
        write_port(0xA0, 0x11);
        
        
        // ICW2 - remap offset address of IDT
        // In x86 protected mode we have to remap the PICs beyond 0x20 because
        // Intel have designated the first 32 interrupts as "reserved" for CPU exceptions
        write_port(0x21, 0x20);
        write_port(0xA1, 0x28);
        
        // ICW3 - setup cascading
        write_port(0x21, 0x00);  
        write_port(0xA1, 0x00);  
        
        // ICW4 - environment info
        write_port(0x21, 0x01);
        write_port(0xA1, 0x01);

        // Initialization finished

        // Mask interrupts
        write_port(0x21, 0xff);
        write_port(0xA1, 0xff);

        // Fill the IDT descriptor
        idt_address = (unsigned long)IDT;
        idt_ptr[0] = (sizeof (struct IDT_entry) * IDT_SIZE) + ((idt_address & 0xffff) << 16);
        idt_ptr[1] = idt_address >> 16;

        load_idt(idt_ptr);

}*/

void init_keyboard() {
        register_interrupt_handler(IRQ1, keyboard_handler_main);
}

/*void kb_init(void) {
        // 0xFD is 11111101 - enables only IRQ1 (Keyboard) 
        write_port(0x21, 0xFD);
}*/



