#include "screen.h"
#include "ports.h"

int get_screen_offset(int col, int row);
int get_cursor_offset();
void set_cursor_offset(int offset);
int handle_scrolling(int cursor_offset);


//------------------------------
// Public Kernel API functions
//------------------------------

void print_at(char* message, int col, int row) {
        // Update cursor if col and row is not negative 
        if (col >= 0 && row >= 0) {
                set_cursor_offset(get_screen_offset(col, row));
        }
        // Loop through each char of the message and print it
        int i = 0;
        while(message[i] != 0) {
                print_char(message[i++], col, row, WHITE_ON_BLACK);
        }
}

void print(char* message) {
        print_at(message, -1, -1);
}

void clear_screen() {
        int row = 0;
        int col = 0;

        // Loop through video memory and wrtie black characters
        for(row = 0; row < MAX_ROWS; row++) {
                for(col = 0; col<MAX_COLS; col++) {
                        print_char(' ', col, row, WHITE_ON_BLACK);
                }
        }

        set_cursor_offset(get_screen_offset(0, 0));
}

//------------------------------
// Private Kernel API functions
//------------------------------

// Print a char on the screen at col, row, or at cursor position
void print_char(char character, int col, int row, char attribute_byte) {
        // Create a byte (char) pointer to the start of video memory
        unsigned char *vidmem = (unsigned char *) VIDEO_ADDRESS;

        // If attribute byte is zero, assume default style
        if(!attribute_byte) {
                attribute_byte = WHITE_ON_BLACK;
        }

        // Get the video memory offset for the screen location
        int offset;
        // If col and row are non-negative, use themm for offset
        if(col >= 0 && row >= 0) {
                offset = get_screen_offset(col, row);
        // Otherwise use current position
        } else {
                offset = get_cursor_offset();
        }

        // Handle the newline character
        if(character == '\n') {
                int rows = offset / (2*MAX_COLS);
                offset = get_screen_offset(79, rows);
                
                // Update the offset
                offset += 2;
                // Handle scrolling adjustment if we reach the bottom of the screen
                //offset = handle_scrolling(offset);

                //Update cursor
                set_cursor_offset(offset);
                //print_char('>', -1, -1, WHITE_ON_BLACK);

        } else if (character == '\b'){
        // Handle backspace character
                if(offset % (MAX_COLS * 2) == 0) {
                        //print_char('>', -1, -1, WHITE_ON_BLACK);

                } else {
                        offset -= 2;
                        vidmem[offset] = ' ';
                        vidmem[offset+1] = attribute_byte;
                        
                        //Update cursor
                        set_cursor_offset(offset);


                }

        } else {
        // Otherwise print the char at the offset
                vidmem[offset] = character;
                vidmem[offset+1] = attribute_byte;
                
                // Update the offset
                offset += 2;
                // Handle scrolling adjustment if we reach the bottom of the screen
                //offset = handle_scrolling(offset);

                //Update cursor
                set_cursor_offset(offset);
        }


}

int get_screen_offset(int col, int row) {
        return ((row * MAX_COLS) + col ) * 2;
}

int get_cursor_offset() {
        // The device uses its control register as an index to select internal registers
        // reg 14: high byte of the cursors offset
        // reg 15: low byte of the cursors offset
        // Once the internal register has been selected we may read or write a byte tot he data register
        port_byte_out(REG_SCREEN_CTRL, 14);
        int offset = port_byte_in(REG_SCREEN_DATA) << 8;
        port_byte_out(REG_SCREEN_CTRL, 15);
        offset += port_byte_in(REG_SCREEN_DATA);
        // Since the cursor offset reported by VGA hardware is the number of chars, 
        // multiply by two a char cell offset
        return offset*2;
}


void set_cursor_offset(int offset) {
        offset /= 2; // Convert from cell offset to char offset
        // Similar to get_cursor, only now we write bytes to internel device registers
        port_byte_out(REG_SCREEN_CTRL, 14);
        port_byte_out(REG_SCREEN_DATA, (unsigned char)(offset >> 8));
        port_byte_out(REG_SCREEN_CTRL , 15);
        port_byte_out(REG_SCREEN_DATA, (unsigned char)(offset & 0xff));
}

